/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mutita.animal;

/**
 *
 * @author Admin
 */
public class Snake extends Reptile{
    private String nickname;

    public Snake(String nickname) {
        super("snake", 4);
        this.nickname=nickname;
    }
    
    @Override
    public void Crawl() {
        System.out.println("snake "+nickname+" crawl");
    }

    @Override
    public void eat() {
        System.out.println("snake "+nickname+" eat");
    }

    //@Override
    //public void walk() {
    //    System.out.println("snake "+nickname+" walk");
   // }

    @Override
    public void speak() {
        System.out.println("snake "+nickname+" speak");
    }

    @Override
    public void sleep() {
       System.out.println("snake "+nickname+" sleep");
    }
}
