/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mutita.animal;

/**
 *
 * @author Admin
 */
public class Crab extends AquaticAnimals {
    private String nickname;
    
    public Crab(String nickname) {
        super("crab");
        this.nickname=nickname;
    }

    @Override
    public void swim() {
        System.out.println("crab "+nickname+" swim");
    }

    @Override
    public void eat() {
       System.out.println("crab "+nickname+" eat");
    }

    

    @Override
    public void speak() {
       System.out.println("crab "+nickname+" speak");
    }

    @Override
    public void sleep() {
        System.out.println("crab "+nickname+" sleep");
    }

    //@Override
   // public void walk() {
    //    System.out.println("crab "+nickname+" walk");
    //}
    
}
