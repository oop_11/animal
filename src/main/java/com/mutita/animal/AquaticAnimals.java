/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mutita.animal;

/**
 *
 * @author Admin
 */
public abstract class AquaticAnimals extends Animal{
    
    public AquaticAnimals(String name) {
        super(name,0);
    }
    public abstract void swim();
}
