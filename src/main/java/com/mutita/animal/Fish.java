/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mutita.animal;

/**
 *
 * @author Admin
 */
public class Fish extends AquaticAnimals {
    private String nickname;
    
    public Fish(String nickname) {
        super("fish");
        this.nickname=nickname;
    }

    @Override
    public void swim() {
        System.out.println("fish "+nickname+" swim");
    }

    @Override
    public void eat() {
       System.out.println("fish "+nickname+" eat");
    }

    

    @Override
    public void speak() {
       System.out.println("fish "+nickname+" speak");
    }

    @Override
    public void sleep() {
        System.out.println("fish "+nickname+" sleep");
    }

   // @Override
   // public void walk() {
   //     System.out.println("fish "+nickname+" walk");
   // }
    
}
