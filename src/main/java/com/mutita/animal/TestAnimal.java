/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mutita.animal;

/**
 *
 * @author Admin
 */
public class TestAnimal {
    public static void main(String[] args) {
        Human h1 = new Human("dang");
        h1.eat();
        h1.walk();
        h1.run();
        
        Cat c1 = new Cat("puriku");
        c1.eat();
        c1.walk();
        c1.run();
        
        Dog d1 = new Dog("mumi");
        d1.eat();
        d1.walk();
        d1.run();
        
        Crocodile croc1 = new Crocodile("bok");
        croc1.eat();
        //croc1.walk();
        croc1.Crawl();
        
        Snake s1 = new Snake("pupu");
        s1.eat();
        //s1.walk();
        s1.Crawl();
        
        Fish  f1 = new Fish("pp");
        f1.eat();
        f1.swim();
        
        Crab  cr1 = new Crab("kiki");
        cr1.eat();
        cr1.swim();
        
        Bat  b1 = new Bat("kaka");
        b1.eat();
        b1.Fly();
        
        Bird  br1 = new Bird("bibi");
        br1.eat();
        br1.Fly();
        
        System.out.println("h1 is a animal ?"+(h1 instanceof Animal));
        System.out.println("h1 is a LandAnimal ?"+(h1 instanceof LandAnimal));
        
        System.out.println("c1 is a animal ?"+(c1 instanceof Animal));
        System.out.println("c1 is a LandAnimal ?"+(c1 instanceof LandAnimal));
        
        System.out.println("d1 is a animal ?"+(d1 instanceof Animal));
        System.out.println("d1 is a LandAnimal ?"+(d1 instanceof LandAnimal));
        
        System.out.println("croc1 is a animal ?"+(croc1 instanceof Animal));
        System.out.println("croc1 is a LandAnimal ?"+(croc1 instanceof Reptile));
        
        System.out.println("s1 is a animal ?"+(s1 instanceof Animal));
        System.out.println("s1 is a LandAnimal ?"+(s1 instanceof Reptile));
        
        System.out.println("f1 is a animal ?"+(f1 instanceof Animal));
        System.out.println("f1 is a LandAnimal ?"+(f1 instanceof AquaticAnimals));
        
        System.out.println("cr1 is a animal ?"+(cr1 instanceof Animal));
        System.out.println("cr1 is a LandAnimal ?"+(cr1 instanceof AquaticAnimals));
        
        System.out.println("b1 is a animal ?"+(b1 instanceof Animal));
        System.out.println("b1 is a LandAnimal ?"+(b1 instanceof Poultry));
        
        System.out.println("br1 is a animal ?"+(br1 instanceof Animal));
        System.out.println("br1 is a LandAnimal ?"+(br1 instanceof Poultry));
        System.out.println("---------------------------------------------------");
        
        Bird bird = new Bird("bird");
        Bat bat = new Bat("bat");
        Plane plane = new Plane("engine1");
        Dog dog = new Dog("tutu");
        Car car = new Car("engine1");
        
        Flyable[] flyable ={bat,plane,bird};
        for(Flyable f : flyable){
            if(f instanceof Plane){
                Plane p = (Plane)f;
                p.startEngine();
                p.run();
        }
            f.Fly();
        }
        
        Runable[] runable = {dog,plane,car};
        for(Runable r: runable){
            r.run();
        }
        
    }
}
